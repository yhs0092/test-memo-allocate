package com.huawei.cse.controller;

import java.util.HashMap;

import javax.ws.rs.core.MediaType;

import org.apache.servicecomb.provider.rest.common.RestSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-11-22T03:37:35.760Z")

@RestSchema(schemaId = "testmemoallocate")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class TestmemoallocateImpl {

  public static final Logger LOGGER = LoggerFactory.getLogger(TestmemoallocateImpl.class);

  @Autowired
  private TestmemoallocateDelegate userTestmemoallocateDelegate;

  private HashMap<String, long[]> cacheMap = new HashMap<>();

  @RequestMapping(value = "/helloworld",
      produces = {"application/json"},
      method = RequestMethod.GET)
  public String helloworld(@RequestParam(value = "name", required = true) String name) {
    LOGGER.info("helloworld() is called, name = [{}]", name);
    return userTestmemoallocateDelegate.helloworld(name);
  }

  @GetMapping(value = "/allocateMemory")
  public String allocateMemory(@RequestParam(value = "clear", defaultValue = "false") boolean clear) {
    LOGGER.info("allocateMemory() is called, clear = [{}]", clear);
    if (clear) {
      cacheMap = null;
      System.gc();
      return "clear";
    }

    try {
      cacheMap = new HashMap<>();
      for (long i = 0; true; ++i) {
        cacheMap.put("key" + i, new long[1024 * 1024]);
      }
    } catch (Throwable t) {
      LOGGER.info("allocateMemory() gets error", t);
    }
    return "allocate";
  }
}
